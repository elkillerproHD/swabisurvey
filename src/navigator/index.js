import React from 'react';
import { Animated, Easing } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import {ReportScreen, SurveyScreen} from '../screens'
import { constants as CONSTANTS } from "../screens/constants";

const AppNavigator = createAppContainer(createStackNavigator(
  {
    [CONSTANTS.SURVEY_CONSTANT]: { screen: SurveyScreen },
    [CONSTANTS.REPORT_CONSTANT]: { screen: ReportScreen },
  },
  {
    initialRouteName: CONSTANTS.SURVEY_CONSTANT,
    headerMode: 'none',
    transitionConfig: () => ({
      transitionSpec: {
        duration: 1000,
        timing: Animated.timing,
        easing: Easing.step0
      }
    }),
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
));

export default AppNavigator;
