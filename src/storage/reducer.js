import {
    WORKING_COOKIE,
    FINISH_WORKING_COOKIE,
    SET_STORAGE,
} from "./actionTypes";

//import {getSession} from "./sessionStorage";

const initialState = {
    workingCookie: false,
    storage: {}
};

export default function reducer(state=initialState, action){
    switch (action.type){
        case WORKING_COOKIE: return {...state,workingCookie: true};
        case FINISH_WORKING_COOKIE: return {...state,workingCookie: false};
        case SET_STORAGE: return {...state, storage: action.payload};
        default: return state
    }
}