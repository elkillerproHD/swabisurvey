import NAME from "./constants";

export const WORKING_COOKIE = NAME + "/WORKING_COOKIE";
export const FINISH_WORKING_COOKIE = NAME + "/FINISH_WORKING_COOKIE";
export const SET_STORAGE = NAME + "/SET_STORAGE";