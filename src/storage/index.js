import reducer from "./reducer";
export {default as storageReducerName} from "./constants";
export default reducer;