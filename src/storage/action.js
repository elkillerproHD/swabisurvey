import { AsyncStorage } from 'react-native';

import {
  WORKING_COOKIE,
  FINISH_WORKING_COOKIE,
  SET_STORAGE,
} from './actionTypes';
import ReducerName from './constants';

export const SetCookie = async (cookieName, cookieValue) => {
  dispatch({ type: WORKING_COOKIE });
  let storageData = {};
  try {
    storageData = await AsyncStorage.getItem(ReducerName);
    storageData = JSON.parse(storageData);
    if (storageData == null) {
      storageData = {};
    }
    storageData[cookieName] = cookieValue;

    try {
      storageData = JSON.stringify(storageData);
      await AsyncStorage.setItem(ReducerName, storageData);
      return Promise.resolve();
    } catch (error) {
      console.log(`Hubo un error guardando la cookie: ${cookieName}`);
      console.log(error);
      return Promise.reject();
    }
  } catch (error) {
    console.log(`Hubo un error obteniendo la cookie: ${cookieName}`);
    console.log(error);
    return Promise.reject();
  }
};

export const GetCookie = async cookieName => {
  dispatch({ type: WORKING_COOKIE });
  let storageData = {};
  try {
    storageData = await AsyncStorage.getItem(ReducerName);
    storageData = JSON.parse(storageData);

    return Promise.resolve(storageData[cookieName]);
  } catch (error) {
    console.log(`Hubo un error obteniendo la cookie: ${cookieName}`);
    console.log(error);
    return Promise.reject(`Hubo un error obteniendo la cookie: ${cookieName}`);
  }
};
