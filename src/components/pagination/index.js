import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import {Normalize, screenHeight, screenWidth} from "../../utils/size";

const strings = {
  next: 'Next',
  back: 'Back',
  finish: 'Finish'
};

class Pagination extends React.Component {

  render () {
    const { props } = this;
    let toRender = [];
    for(let k = 0; k < props.length; k ++ ) {
      if(props.actualStep === k){
        toRender.push(
          <View key={`Dot ${k}`} style={{ width: screenWidth * 0.035, height: screenWidth * 0.035, borderRadius: screenWidth * 0.035, backgroundColor: '#FFDC14'}} />
        )
      } else {
        toRender.push(
          <View key={`Dot ${k}`} style={{ width: screenWidth * 0.035, height: screenWidth * 0.035, borderRadius: screenWidth * 0.035, backgroundColor: '#000'}} />
        )
      }
    }
    return(
      <View style={{width: '100%', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', paddingHorizontal: '2%'}}>

        {
          props.actualStep !== 0
          ? (
              <TouchableOpacity onPress={props.backStep} style={{width: '25%', backgroundColor: '#FFDC14', alignItems: 'center', justifyContent: 'center', height: screenHeight * 0.05, borderRadius: screenWidth * 0.1}}>
                <Text style={{ fontSize: Normalize(15), color: '#000' }}>{strings.back}</Text>
              </TouchableOpacity>
            )
          : (
              <TouchableOpacity style={{width: '25%', height: screenHeight * 0.05}}>
              </TouchableOpacity>
            )
        }
        <View style={{width: '33%', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center'}}>
          {
            toRender.map((render) => (render))
          }
        </View>
        {
          props.actualStep === props.length - 1
          ? props.canContinue
            ? (
              <TouchableOpacity onPress={props.FinishAndCaptureData} style={{width: '25%', backgroundColor: '#FFDC14', alignItems: 'center', justifyContent: 'center', height: screenHeight * 0.05, borderRadius: screenWidth * 0.1}} >
                <Text style={{ fontSize: Normalize(15), color: '#000' }}>{strings.finish}</Text>
              </TouchableOpacity>
            )
            : (
              <TouchableOpacity style={{width: '25%', backgroundColor: '#cacaca', alignItems: 'center', justifyContent: 'center', height: screenHeight * 0.05, borderRadius: screenWidth * 0.1}} >
                <Text style={{ fontSize: Normalize(15), color: '#000' }}>{strings.finish}</Text>
              </TouchableOpacity>
            )
          :
            props.canContinue
            ? (
              <TouchableOpacity onPress={props.nextStep} style={{width: '25%', backgroundColor: '#FFDC14', alignItems: 'center', justifyContent: 'center', height: screenHeight * 0.05, borderRadius: screenWidth * 0.1}} >
                <Text style={{ fontSize: Normalize(15), color: '#000' }}>{strings.next}</Text>
              </TouchableOpacity>
            )
            : (
                <TouchableOpacity style={{width: '25%', backgroundColor: '#cacaca', alignItems: 'center', justifyContent: 'center', height: screenHeight * 0.05, borderRadius: screenWidth * 0.1}} >
                  <Text style={{ fontSize: Normalize(15), color: '#000' }}>{strings.next}</Text>
                </TouchableOpacity>
              )
        }

      </View>
    )
  }
}

export default Pagination
