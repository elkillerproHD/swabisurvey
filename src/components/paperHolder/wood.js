import React from 'react'
import { View, StyleSheet } from 'react-native'
import {GetWidth, screenHeight, screenWidth} from "../../utils/size";
import {getStatusBarHeight} from "react-native-status-bar-height";


function WoodBackground(props){
  return(
    <View style={styles.container}>
      <View style={styles.wood}>
        {props.children}
      </View>
    </View>
  )
}

export default WoodBackground;

const styles = StyleSheet.create({
  container: {
    width: screenWidth,
    height: screenHeight,
    backgroundColor: '#cacaca',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingTop: screenWidth * 0.15
  },
  wood: {
    width: screenWidth * 0.98,
    height: screenHeight * .9 - screenWidth * 0.02 - getStatusBarHeight(),
    backgroundColor: '#8b5a2b',
    borderRadius: GetWidth(10),
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingBottom: screenWidth * 0.02,
    borderColor: '#fff',
    borderWidth: 1,
    position: 'relative'
  }
});
