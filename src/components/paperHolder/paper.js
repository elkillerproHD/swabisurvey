import React from 'react';
import { View } from 'react-native'
import {GetHeight, GetWidth} from "../../utils/size";

function Paper(props){
  return(
    <View style={{ paddingTop: GetHeight(45), backgroundColor: '#fff', width: '95%', height: '95%', borderWidth: GetWidth(1), borderBottomWidth: GetWidth(2), borderColor: '#000' }}>
      { props.children }
    </View>
  )
}

export default Paper
