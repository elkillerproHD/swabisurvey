import React from 'react';
import WoodBackground from "./wood";
import Paper from "./paper";
import ClipHolder from "./clipHolder";
import {Text, Keyboard, TouchableWithoutFeedback, View} from "react-native";
import {GetHeight, Normalize, screenHeight, screenWidth} from "../../utils/size";
import {getStatusBarHeight} from "react-native-status-bar-height";
import InputScrollView from "react-native-input-scroll-view";
import Pagination from "../pagination";

class PaperHolder extends React.Component {

  KeyboardDidShow = (e) => {
    const avoidheight = e.endCoordinates.height;
    this.setState({
      keyboardHeightAvoid: avoidheight + getStatusBarHeight(),
      keyboardAppears: true
    });
  };

  KeyboardDidHide = (e) => {
    this.setState({
      keyboardHeightAvoid: getStatusBarHeight(),
      keyboardAppears: false
    });
  };

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.KeyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.KeyboardDidHide);
  }

  render() {
    const { props } = this;
    return (
      <View style={{
        width: screenWidth,
        height: screenHeight,
        backgroundColor: '#cacaca'
      }}>
      <TouchableWithoutFeedback
        onPress={() => {
          Keyboard.dismiss();
        }}
        accessible
      >
        {
          props.scroll
          ? (
              <View style={{
                width: screenWidth,
                height: screenHeight,
                position: 'relative'
              }}>
              <InputScrollView
                keyboardShouldPersistTaps="always"
                keyboardOffset={GetHeight(1200)}
              >
                  <WoodBackground>
                    <ClipHolder/>
                    <Paper>
                      <Text style={{
                        width: '100%',
                        textAlign: 'center',
                        fontSize: Normalize(42),
                        color: '#000',
                        fontFamily: 'Pacifico-Regular'
                      }}>Swaby Surbey</Text>
                      {
                        props.children
                      }
                    </Paper>
                    <View style={{paddingHorizontal: '3%', width: '100%', position: 'absolute', bottom: GetHeight(18), left: 0}}>
                      <Pagination FinishAndCaptureData={props.FinishAndCaptureData} canContinue={props.canContinue} actualStep={props.actualStep} nextStep={props.nextStep} backStep={props.backStep} length={props.length}/>
                    </View>
                  </WoodBackground>
                <View style={{width: '100%', height: GetHeight(200)}} />
              </InputScrollView>
              </View>

            )
          : (
              <View style={{
                width: screenWidth,
                height: screenHeight,
                position: 'relative'
              }}>

                <WoodBackground>
                  <ClipHolder/>
                  <Paper>
                    <Text style={{
                      width: '100%',
                      textAlign: 'center',
                      fontSize: Normalize(42),
                      color: '#000',
                      fontFamily: 'Pacifico-Regular'
                    }}>Swaby Surbey</Text>
                    {
                      props.children
                    }
                  </Paper>
                  <View style={{paddingHorizontal: '3%', width: '100%', position: 'absolute', bottom: GetHeight(18), left: 0}}>
                    <Pagination FinishAndCaptureData={props.FinishAndCaptureData} canContinue={props.canContinue} actualStep={props.actualStep} nextStep={props.nextStep} backStep={props.backStep} length={props.length}/>
                  </View>
                </WoodBackground>
              </View>
            )
        }

      </TouchableWithoutFeedback>
      </View>
    )
  }
}

PaperHolder.defaultProps = {
  scroll: false
};

export default PaperHolder
