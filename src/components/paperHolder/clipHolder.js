import React from 'react'
import { View, StyleSheet } from 'react-native'
import {GetHeight, GetWidth, screenHeight, screenWidth} from "../../utils/size";

function ClipHolder (props) {
  return(
    <View style={styles.container}>
      <View style={styles.top}>
        <View style={styles.circle}/>
      </View>
      <View style={styles.middle}/>
      <View style={styles.bottom}>
        <View style={styles.circleBottom}/>
      </View>
    </View>
  )
}

export default ClipHolder

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    position:'absolute',
    top: GetHeight(45) * -1,
    left: 'auto',
    zIndex: 1
  },
  top: {
    width: screenWidth * 0.1,
    height: screenWidth * 0.1,
    backgroundColor: '#808080',
    borderTopLeftRadius: GetWidth(screenWidth * 0.1),
    borderTopRightRadius: GetWidth(screenWidth * 0.1),
    borderWidth: GetWidth(1),
    borderColor: '#000',
    borderBottomWidth: 0,
    zIndex: 2,
    marginBottom: -3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  middle: {
    width: screenWidth * 0.4,
    height: screenWidth * 0.1,
    backgroundColor: '#808080',
    borderWidth: GetWidth(1),
    borderColor: '#000',
    borderTopLeftRadius: GetWidth(screenWidth * 0.015),
    borderTopRightRadius: GetWidth(screenWidth * 0.015),
  },
  bottom: {
    width: screenWidth * 0.6,
    height: screenWidth * 0.1,
    backgroundColor: '#808080',
    borderTopLeftRadius: GetWidth(8),
    borderTopRightRadius: GetWidth(8),
    borderWidth: GetWidth(1),
    borderColor: '#000',
    zIndex: 2,
    marginTop: -5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  circleBottom: {
    width: '80%',
    height: '30%',
    backgroundColor: '#fff',
    borderRadius: GetWidth(8),
    borderWidth: GetWidth(1),
    borderColor: '#000',
  },
  circle: {
    width: screenWidth * 0.04,
    height: screenWidth * 0.04,
    backgroundColor: '#fff',
    borderRadius: screenWidth * 0.015,
    borderWidth: GetWidth(1),
    borderColor: '#000',
  },
});
