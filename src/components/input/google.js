import React from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Animated,
  Platform,
  Keyboard,
  Text
} from 'react-native';
import { GetHeight, Normalize } from '../../utils/size';

export class GoogleInput extends React.Component {
  static defaultProps = {
    leftIcon: false,
    leftIconManual: false,
    rightIcon: undefined,
    passwordInput: false,
    loadRef: () => {},
    onPressRightIcon: () => {},
    onPress: () => {},
    onFocus: () => {},
    onBlur: () => {},
    onChangeText: () => {},
    onClear: () => {},
    noClear: false,
    padding: false,
    error: undefined,
    placeholder: undefined,
    value: undefined,
    secureTextEntry: false,
    keyboardType: 'default',
    autoFocus: false,
    googleAnimation: true,
    editable: true,
    focuseable: true,
    topText: undefined,
    containerStyle: {},
    lineWhite: false,
    colorRightIcon: '#000',
    autoCapitalize: 'none',
    controlled: true,
    autoCompleteType: 'off',
    multiline: false
  };

  constructor(props) {
    super(props);
    this.lineAnimation = new Animated.Value(0);
    this.state = {
      lineWidth: 0,
      focus: false
    };
    if (props.value) {
      this.topTextFloatDistance = new Animated.Value(0);
      this.leftTextFloatDistance = new Animated.Value(0);
      this.state.topTextFloatDistance = 0;
      this.state.leftTextFloatDistance = 0;
    } else {
      this.topTextFloatDistance = new Animated.Value(Normalize(25));
      this.leftTextFloatDistance = new Animated.Value(0);
      this.state.topTextFloatDistance = Normalize(25);
      this.state.leftTextFloatDistance = 0;
    }
  }

  componentDidMount() {
    this.lineAnimation.addListener((lineWidth) => {
      this.setState({
        lineWidth: `${lineWidth.value.toString()}%`
      });
    });
    this.topTextFloatDistance.addListener((topTextFloatDistance) => {
      this.setState({
        topTextFloatDistance: topTextFloatDistance.value
      });
    });
    this.leftTextFloatDistance.addListener((leftTextFloatDistance) => {
      this.setState({
        leftTextFloatDistance: leftTextFloatDistance.value
      });
    });
    const { props, inputRef } = this;
    props.loadRef(inputRef);
  }

  FocusAnimation = () => {
    Animated.parallel([
      Animated.timing(this.lineAnimation, {
        toValue: 100,
        duration: 100
      }).start(),
      Animated.timing(this.topTextFloatDistance, {
        toValue: 0,
        duration: 100
      }).start(),
      Animated.timing(this.leftTextFloatDistance, {
        toValue: 0,
        duration: 100
      }).start()
    ]).start();
    this.setState({
      focus: true
    });
  };

  onFocus = () => {
    const { props } = this;
    this.FocusAnimation();
    props.onFocus();
    this.setState({
      focus: true
    });
  };

  AnimationBlurWithNOTValue = () => {
    const { props } = this;
    Animated.parallel([
      Animated.timing(this.lineAnimation, {
        toValue: 0,
        duration: 200
      }).start(),
      Animated.timing(this.topTextFloatDistance, {
        toValue: props.value ? 0 : Normalize(25),
        duration: 200
      }).start(),
      Animated.timing(this.leftTextFloatDistance, {
        toValue: props.value ? 0 : 0,
        duration: 200
      }).start()
    ]).start();
    this.setState({
      focus: false
    });
  };

  AnimationBlurWithValue = () => {
    const { props } = this;
    props.onBlur();
    Animated.parallel([
      Animated.timing(this.lineAnimation, {
        toValue: 0,
        duration: 200
      }).start()
    ]).start();
    this.setState({
      focus: false
    });
  };

  onBlur = () => {
    const { props } = this;
    props.onBlur();
    if (!props.value) {
      this.AnimationBlurWithNOTValue();
      return;
    }
    if (props.value.length < 1) {
      this.AnimationBlurWithNOTValue();
      return;
    }
    this.AnimationBlurWithValue();
  };

  Focus = () => {
    const { props, inputRef } = this;
    if (props.focuseable) {
      if (props.editable) {
        inputRef.focus();
      } else {
        this.FocusAnimation();
      }
    }
  };

  Blur = () => {
    const { inputRef } = this;
    inputRef.blur();
    Keyboard.dismiss();
  };

  onPress = () => {
    const { props, inputRef } = this;
    if (props.focuseable) {
      if (props.editable) {
        inputRef.focus();
      } else {
        this.FocusAnimation();
      }
    }
    props.onPress();
  };

  render() {
    const { props, state } = this;
    return (
      <View
        style={[
          props.error
            ? { ...styles.containerError, ...props.containerStyle }
            : { ...styles.container, ...props.containerStyle },
          props.padding && {
            paddingLeft: props.padding,
            paddingRight: props.padding
          }
        ]}
      >
        <View style={styles.subContainer}>
          <View style={styles.inputContainer}>
            {/* <View style={styles.leftContainer} /> */}
            <View style={styles.textInputContainer}>
              <TextInput
                ref={inputRef => (this.inputRef = inputRef)}
                accessible={false}
                placeholderColor={'#000'}
                placeholder={
                  props.googleAnimation ? undefined : props.placeholder
                }
                style={styles.inputStyle}
                value={props.value}
                onFocus={this.onFocus}
                onBlur={this.onBlur}
                onChangeText={props.onChangeText}
                keyboardType={props.keyboardType}
                secureTextEntry={props.secureTextEntry}
                autoFocus={props.autoFocus}
                editable={props.editable}
                returnKeyType="next"
                autoCapitalize={props.autoCapitalize}
                autoCompleteType={props.autoCompleteType}
                multiline={props.multiline}
              />
            </View>
          </View>
          <View
            style={
              props.error
                ? styles.lineContainerError
                : props.lineWhite
                  ? styles.lineContainerWhite
                  : styles.lineContainer
            }
          >
            <Animated.View
              style={{
                width: state.lineWidth,
                height: 2,
                backgroundColor: props.error
                  ? '#000'
                  : '#FFDC14',
                borderRadius: 2
              }}
            />
          </View>
          {props.error && (
            <Text
              style={{
                color: '#000',
                textAlign: 'left',
                marginTop: 3,
                position: 'absolute',
                bottom: GetHeight(20) * -1,
                left: 0,
                fontSize: Normalize(18),
              }}
            >
              {props.error}
            </Text>
          )}
          {!props.editable ? (
            <TouchableOpacity
              style={styles.touchableFloat}
              onPress={this.onPress}
              onFocus={this.onFocus}
              onBlur={this.onBlur}
            />
          ) : (
            !state.focus && (
              <TouchableOpacity
                style={styles.touchableFloat}
                onPress={this.onPress}
                onFocus={this.onFocus}
                onBlur={this.onBlur}
              />
            )
          )}

          {
            <Animated.View
              style={{
                top: props.googleAnimation ? state.topTextFloatDistance : 0,
                left: props.googleAnimation ? state.leftTextFloatDistance : 0,
                position: 'absolute',
                zIndex: 1
              }}
            >
              <Text
                style={[
                  props.googleAnimation
                    ? state.focus
                    ? styles.floatTextFocus
                    : styles.floatText
                    : styles.floatText
                  ,{
                    fontSize: Normalize(16),
                  }
                ]}
                type="regular"
                size={props.googleAnimation ? (state.focus ? 1 : 2) : 1}
              >
                {props.googleAnimation ? props.placeholder : props.topText}
              </Text>
            </Animated.View>
          }
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textInputContainer: {
    height: Normalize(35),
    width: '100%',
    marginRight: -22,
    paddingRight: 22
  },
  rightIconContainer: {
    width: 20,
    height: Normalize(30),
    paddingTop: Normalize(10),
    paddingBottom: Normalize(10),
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 4
  },
  inputStyle: {
    height: Normalize(28),
    paddingTop: Platform.OS === 'ios' ? 0 : 0,
    marginTop: Platform.OS === 'ios' ? 6 : 10,
    lineHeight: Normalize(16),
    fontSize: Normalize(14),
    zIndex: 1,
    fontFamily: 'NunitoSans-Regular',
    color: '#000'
  },
  floatText: {
    color: '#000',
    lineHeight: Normalize(17),
    fontSize: Normalize(14)
  },
  floatTextFocus: {
    color: '#000',
    lineHeight: Normalize(16),
    fontSize: Normalize(13)
  },
  subContainer: {
    width: '100%',
    height: '100%',
    position: 'relative',
    justifyContent: 'flex-end'
  },
  container: {
    width: '100%',
    height: Normalize(42)
  },
  containerError: {
    width: '100%',
    height: Normalize(42)
  },
  leftContainer: {
    width: 10,
    height: Normalize(35)
  },
  touchableFloat: {
    width: '100%',
    height: Platform.OS === 'ios' ? Normalize(52) : Normalize(42),
    lineHeight: Platform.OS === 'ios' ? Normalize(52) : Normalize(42),
    top: 0,
    left: 0,
    position: 'absolute',
    zIndex: 3
  },
  inputContainer: {
    width: '100%',
    height: Normalize(30),
    flexDirection: 'row'
  },
  lineContainer: {
    width: '100%',
    height: 2,
    backgroundColor: '#000',
    borderRadius: 2
  },
  lineContainerWhite: {
    width: '100%',
    height: 2,
    backgroundColor: '#fff',
    borderRadius: 2
  },
  lineContainerError: {
    width: '100%',
    height: 2,
    backgroundColor: '#000',
    borderRadius: 2
  }
});
