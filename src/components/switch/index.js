import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import Style from './style';
import {GetHeight, Normalize} from '../../utils/size';

// Tiene dos estados horizontal y vertical...
// Este componente recive
// {
//     onChange: (funcion) Se ejecuta la funcion cada vez que se cambie el valor del boton,
//     vertical: (bool) Los botones pasan a ser verticales en vez de horizontales,
//     card: (bool) Al igual que vertical hace que los botones pasen a ser verticales pero ademas se puede utilizar rightIcon, rightText y rightBlack
//     selected: (int) Numero del index, key o id (el key del array que le corresponda) con que iniciara el seleccionado
//     text: el texto a mostrar sobre (arriba) todos los botones
//     textStyle: estilo a colocar al texto en cuestion
//     buttons:[
//         {
//             name: Es lo que le da el nombre al texto que se visualizara en el interior
//             key: Es para obtener un identificador unico, no hace falta colocarlo en el json ya que se obtiene por orden de manera automatica
//             rightIcon: Icono que va a la derecha si esta la prop rightText, no se muestra el icono si no el texto, el texto tiene mayor "poder"
//             rightText: Texto que se va a ver a la derecha del boton
//             rightBlack: si este booleano esta activo aunque este seleccionado el boton, el texto seguira en color negro, si se vuelve celeste
//             cantSelect: Si es verdadero no se visualizara efecto de seleccion
//             onPress: funcion unica que se llama si se presiona un cierto boton, se ejecuta a parte del retorno de buttonPressed en onChange
//         }
//     ]
// }

class ButtonSwitch extends React.Component {
  state = {
    selected: this.props.selected,
  };

  ButtonPressed = (value, index) => {
    const { props } = this;
    if(!props.manual){
      this.setState({
        selected: index
      });
    }
    this.props.onChange(value, index);
  };

  checkVertical = () => !!(this.props.vertical || this.props.card);

  constructor(props) {
    super(props);
    const vertical = this.checkVertical();
    this.state = {
      selected: this.props.selected,
      vertical,
    };
  }
  GetSizeProgressIcon = () => {
    if (this.props.vertical || this.props.card) {
      return '100%';
    }
    let division = (100 / this.props.buttons.length);
    division = `${division.toString()}%`;
    return division;
  };
  styles = Style(this);
  ButtonSwitch = (name, key, rightText = false, rightIcon = false, rightBlack = false, cantSelect = false, onPress = () => {}) => {
    const { props } = this;
    const selected = props.manual ? props.selected : this.state.selected.toString();
    let width = this.props.buttons.length - 1;
    width = width.toString();
    let onlyOne = false;
    if (!rightIcon && !rightText) {
      onlyOne = true;
    }
    if (this.props.vertical || this.props.card) {
      return (
        <TouchableOpacity
          style={[!cantSelect ? selected === key ? this.styles.buttonSelected : this.styles.buttonUnselected : this.styles.buttonUnselected, this.styles.buttonVertical, this.props.card && this.styles.buttonCard, this.props.buttonStyle, selected === key && this.props.buttonSelectedStyle]}
          key={key}
          onPress={() => {
            this.ButtonPressed(name, key);
            onPress(name, key);
          }}
        >
          <View style={this.styles.containerText}>
            <Text
              style={[
                !cantSelect
                  ? selected === key
                    ? {
                      color: '#FFDC14',
                      textAlign: 'center'
                    }
                    : {
                      color: '#000',
                      textAlign: 'center'
                    }
                  : {
                    color: '#000',
                    textAlign: 'center'
                  }
              ,{
                fontSize: Normalize(18),
                fontFamily: 'Pacifico-Regular'
              }
              ]}
            >
              {name}
            </Text>
            {
              this.props.card
              && rightText
                ? (
                  <Text
                    style={[
                      rightBlack
                        ? {
                          color: '#000',
                          textAlign: 'center'
                        }
                        : selected === key
                          ? {
                            color: '#FFDC14',
                            textAlign: 'center'
                          }
                          : {
                            color: '#000',
                            textAlign: 'center'
                          }
                      ,{
                        fontSize: Normalize(18),
                        fontFamily: 'Pacifico-Regular'
                      }
                    ]}
                  >
                    {rightText}
                  </Text>
                )
                : rightIcon
                  && rightIcon
            }
          </View>

        </TouchableOpacity>
      );
    }
    switch (key) {
      case '0':
        return (
          <TouchableOpacity
            style={[selected === key ? this.styles.buttonSelected : this.styles.buttonUnselected, this.styles.buttonLeft]}
            key={key}
            onPress={() => {
              this.ButtonPressed(name, key);
              onPress(name, key);
            }}
          >
            <Text
              style={[
                selected === key
                  ? {
                    color: '#FFDC14',
                    textAlign: 'center'
                  }
                  : {
                    color: '#000',
                    textAlign: 'center'
                  },{
                  fontSize: Normalize(18),
                  fontFamily: 'Pacifico-Regular'
                }]
              }
            >
              {name}
            </Text>
          </TouchableOpacity>
        );
        break;
      case width:
        return (
          <TouchableOpacity
            style={[selected === key ? this.styles.buttonSelected : this.styles.buttonUnselected, this.styles.buttonRight]}
            key={key}
            onPress={() => {
              this.ButtonPressed(name, key);
              onPress(name, key);
            }}
          >
            <Text
              style={[
                selected === key
                  ? {
                    color: '#FFDC14',
                    textAlign: 'center'
                  }
                  : {
                    color: '#000',
                    textAlign: 'center'
                  }
                  ,{
                    fontSize: Normalize(18),
                    fontFamily: 'Pacifico-Regular'
                  }
              ]}
            >
              {name}
            </Text>
          </TouchableOpacity>
        );
        break;
      default:
        return (
          <TouchableOpacity
            style={[selected === key ? this.styles.buttonSelected : this.styles.buttonUnselected, this.styles.buttonMid]}
            key={key}
            onPress={() => {
              this.ButtonPressed(name, key);
              onPress(name, key);
            }}
          >
            <Text
                key={key}
              style={[
                selected === key
                  ? {
                    color: '#FFDC14',
                    textAlign: 'center'
                  }
                  : {
                    color: '#c0e0ff',
                    textAlign: 'center'
                  }
                  ,{
                    fontSize: Normalize(18),
                    fontFamily: 'Pacifico-Regular'
                  }
              ]}
              type="regular"
              size={1}
            >
              {name}
            </Text>
          </TouchableOpacity>
        );
    }
  };

    ButtonsRender = () => {
      const buttonsIcon = [];
      for (let i = 0; i < this.props.buttons.length; i++) {
        if (this.props.selected >= i + 1) {
          buttonsIcon.push(this.ButtonSwitch(this.props.buttons[i].name, i.toString(), this.props.buttons[i].rightText, this.props.buttons[i].rightIcon, this.props.buttons[i].rightBlack, this.props.buttons[i].cantSelect, this.props.buttons[i].onPress));
        } else {
          buttonsIcon.push(this.ButtonSwitch(this.props.buttons[i].name, i.toString(), this.props.buttons[i].rightText, this.props.buttons[i].rightIcon, this.props.buttons[i].rightBlack, this.props.buttons[i].cantSelect, this.props.buttons[i].onPress));
        }
      }
      return buttonsIcon;
    };


    render() {
      const { ButtonsRender, props, styles } = this;
      return (
        <View style={{...styles.container, ...props.containerStyles}}>
          <View style={this.state.vertical ? this.styles.subContainerVertical : this.styles.subContainer}>
            <ButtonsRender />
          </View>
        </View>
      );
    }
}

ButtonSwitch.defaultProps = {
  selected: 0,
  text: false,
  vertical: false,
  card: false,
  manual: false,
  containerStyles: {},
  onChange: () => {
  },
};

export default ButtonSwitch;
