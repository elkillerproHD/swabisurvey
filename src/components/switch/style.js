import { StyleSheet } from 'react-native';
import {GetHeight, Normalize} from "../../utils/size";

export default function Style(clase) {
  return StyleSheet.create({
    container: {
      width: '100%',
      alignItems: 'center',
      height: GetHeight(45),
      marginVertical: 5
    },
    subContainerVertical: {
      width: '100%',
      flexDirection: 'column',
      justifyContent: 'flex-start',
      alignItems: 'center',
    },
    subContainer: {
      width: '100%',
      flexDirection: 'row',
      justifyContent: 'flex-start',
      alignItems: 'center',

    },
    buttonSelected: {
      borderColor: '#FFDC14',
      width: clase.GetSizeProgressIcon(),
      height: GetHeight(45),
      borderWidth: 2,
      marginHorizontal: -0.5,
      alignItems: 'center',
      justifyContent: 'center'
    },
    buttonUnselected: {
      width: clase.GetSizeProgressIcon(),
      height: GetHeight(45),
      borderColor: '#000',
      marginHorizontal: -0.5,
      borderWidth: 1,
      alignItems: 'center',
      justifyContent: 'center'
    },
    buttonLeft: {
      borderBottomLeftRadius: 4,
      borderTopLeftRadius: 4,
      alignItems: 'center',
      justifyContent: 'center'
    },
    buttonRight: {
      borderBottomRightRadius: 4,
      borderTopRightRadius: 4,
      alignItems: 'center',
      justifyContent: 'center'
    },
    buttonMid: {
      alignItems: 'center',
      justifyContent: 'center'
    },
    buttonVertical: {
      borderBottomRightRadius: 4,
      borderTopRightRadius: 4,
      borderBottomLeftRadius: 4,
      borderTopLeftRadius: 4,
      marginVertical: 8
    },
    buttonCard: {
      justifyContent: 'space-between'
    },
    progressIconEmpty: {
      width: clase.GetSizeProgressIcon(),
      height: 2,
      borderRadius: 2,
      backgroundColor: '#cdcdcd'
    },
    textInput: {
      color: '#000',
      fontSize: Normalize(14),
      textAlign: 'center',
      alignItems: 'center',
    },
    textInputBlack: {
      color: '#000',
      fontSize: Normalize(14),

      textAlign: 'center',
      alignItems: 'center',
    },
    textInputSelected: {
      color: '#FFDC14',
      textAlign: 'center',
      alignItems: 'center',
    },
    oneText: {
      width: '100%',
      fontSize: 14,
      lineHeight: GetHeight(23),
      // height: 16,
      textAlign: 'center',
      alignItems: 'center',
    },
    containerText: {
      height: '100%',
      width: '95%',
      justifyContent: 'space-between',
      alignItems: 'center',
      flexDirection: 'row',

    },
  });
}
