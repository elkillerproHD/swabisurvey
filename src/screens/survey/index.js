import React from "react";
import {Text, TouchableOpacity, Image, AsyncStorage, BackHandler, Alert} from 'react-native';
import PaperHolder from "../../components/paperHolder";
import {
  SurveyStep1,
  SurveyStep2,
  SurveyStep3,
  SurveyStep4,
  SurveyStep5,
  SurveyStep6,
} from './steps'
import {GetHeight, GetWidth, Normalize} from "../../utils/size";
import ClipHolder from "../../components/paperHolder/clipHolder";
import Paper from "../../components/paperHolder/paper";
import WoodBackground from "../../components/paperHolder/wood";
import PlusIcon from '../../../images/add.png'
import ReportIcon from '../../../images/statistics.png'
import {SetCookie} from "../../storage/action";
import ReducerName from "../../storage/constants";
import {WORKING_COOKIE} from "../../storage/actionTypes";
import {constants as CONSTANTS} from "../constants";

const strings = {
  addNew: 'Add New',
  reports: 'Reports'
};

class SurveyScreen extends React.Component {

  state = {
    step: 0,
    hasWeb: -1,
    hasMobileApp: -1,
    name: '',
    jobTitle: '',
    company: '',
    phone: '',
    email: '',
    wantToBeContacted: -1,
    descriptionTechNeeded: '',
    ceoName: '',
    canContinue: false,
    addNewSurvey: false,
  };

  nextStep = () => {
    let {step} = this.state;
    step = step + 1;
    this.setState({
      step,
      canContinue: false,
    });
  };

  backStep = () => {
    let {step} = this.state;
    step = step - 1;
    this.setState({
      step,
      canContinue: false,
    })
  };

  BackPress = () => {
    const { state } = this;
    if(state.step !== 0){
      this.backStep();
    } else {
      this.setState({
        step: 0,
        hasWeb: -1,
        hasMobileApp: -1,
        name: '',
        jobTitle: '',
        company: '',
        phone: '',
        email: '',
        wantToBeContacted: -1,
        descriptionTechNeeded: '',
        ceoName: '',
        canContinue: false,
        addNewSurvey: false,
      });
    }
    return true;
  };

  componentDidMount() {
    this.backPress = BackHandler.addEventListener(
      'hardwareBackPress',
      this.BackPress
    );
  }

  CheckForm = () => {
    const toCheck = [
      "name",
      "jobTitle",
      "company",
      "phone",
      "email",
    ];
    let canContinue = true;
    toCheck.forEach((value) => {
      const { state } = this;
      if(state[value].length < 1){
        canContinue = false;
      }
    });
    this.setState({canContinue});
  };

  setData = (name, value) => {
    const { state } = this;
    this.setState({
      [name]: value
    });
    switch (state.step) {
      case 0: if(value !== -1){this.setState({canContinue: true})}else{this.setState({canContinue: false})}break;
      case 1: if(value !== -1){this.setState({canContinue: true})}else{this.setState({canContinue: false})}break;
      case 2: this.CheckForm(); break;
      case 3: if(value !== -1){this.setState({canContinue: true})}else{this.setState({canContinue: false})}break;
      case 4: if(value.length > 0){this.setState({canContinue: true})}else{this.setState({canContinue: false})} break;
      case 5:if(value.length > 0){this.setState({canContinue: true})}else{this.setState({canContinue: false})} break;
      default: break;
    }
  };

  steps = [
    <SurveyStep1 value={this.state.hasWeb} setData={(value) => {this.setData('hasWeb', value)}} />,
    <SurveyStep2 value={this.state.hasMobileApp} setData={(value) => {this.setData('hasMobileApp', value)}} />,
    <SurveyStep3
      name={this.state.name}
      jobTitle={this.state.jobTitle}
      company={this.state.company}
      phone={this.state.phone}
      email={this.state.email}
      setData={(name, value) => {this.setData(name, value)}}
    />,
    <SurveyStep4 value={this.state.wantToBeContacted} setData={(value) => {this.setData('wantToBeContacted', value)}} />,
    <SurveyStep5 value={this.state.descriptionTechNeeded} setData={(value) => {this.setData('descriptionTechNeeded', value)}} />,
    <SurveyStep6 value={this.state.ceoName} setData={(value) => {this.setData('ceoName', value)}} />,
  ];

  SetCookie = async (cookieValue) => {
    let storageData = {};
    try {
      storageData = await AsyncStorage.getItem('report');
      storageData = JSON.parse(storageData);
      console.log('storageData1')
      console.log(storageData)
      if (storageData == null) {
        storageData = {};
      }
      console.log('storageData2')
      console.log(JSON.stringify(storageData))
      console.log(`if(storageData['report'])`);
      console.log(!!storageData['report'])
      if(storageData['report']){
        if(storageData['report'].length > 0){
          storageData['report'].push(cookieValue);
        } else {
          storageData['report']  = [];
          storageData['report'][0] = cookieValue;
        }
      } else {
        storageData['report']  = [];
        storageData['report'][0] = cookieValue;
      }
      try {
        storageData = JSON.stringify(storageData);
        await AsyncStorage.setItem('report', storageData);
        return Promise.resolve();
      } catch (error) {
        console.log(`Hubo un error guardando la cookie: ${'report'}`);
        console.log(error);
        return Promise.reject();
      }
    } catch (error) {
      console.log(`Hubo un error obteniendo la cookie: ${'report'}`);
      console.log(error);
      return Promise.reject();
    }
  };

  FinishAndCaptureData = () => {
    const { state } = this;
    console.log(state);
    const hasWeb = [
      {
        name: 'Yes',
      },
      {
        name: 'No',
      },
    ];
    const hasMobileApp = [
      {
        name: 'Yes',
      },
      {
        name: 'No',
      },
    ];
    const wantToBeContacted = [
      {
        name: 'Yes',
      },
      {
        name: 'No',
      },
    ];

    const data = {
      hasWeb: hasWeb[parseInt(state.hasWeb)].name,
      hasMobileApp: hasMobileApp[parseInt(state.hasMobileApp)].name,
      name: state.name,
      jobTitle: state.jobTitle,
      company: state.company,
      phone: state.phone,
      email: state.email,
      wantToBeContacted: wantToBeContacted[parseInt(state.wantToBeContacted)].name,
      descriptionTechNeeded: state.descriptionTechNeeded,
      ceoName: state.ceoName,
    };

    this.SetCookie(data).then(() => {
      this.setState({
        step: 0,
        hasWeb: -1,
        hasMobileApp: -1,
        name: '',
        jobTitle: '',
        company: '',
        phone: '',
        email: '',
        wantToBeContacted: -1,
        descriptionTechNeeded: '',
        ceoName: '',
        canContinue: false,
        addNewSurvey: false,
      });
    }).catch(() => {
      Alert.alert(
        'An error has occurred. Try again',
        '',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        {cancelable: false},
      );
    });



  };

  SetAddNewScreen = () => {
    this.setState({
      addNewSurvey: true
    });
  };

  SetReportScreen = () => {
    const { props } = this;
    props.navigation.navigate(CONSTANTS.REPORT_CONSTANT)
  };

  render() {
    const {nextStep, backStep, FinishAndCaptureData} = this;
    const { state } = this;
    const ActualStep = this.steps[state.step];
    let canScroll = false;
    if(state.step === 4 || state.step === 2){
      canScroll = true;
    }
    if(!state.addNewSurvey){
      return(
        <WoodBackground>
          <ClipHolder/>
          <Paper>
            <Text style={{
              width: '100%',
              textAlign: 'center',
              fontSize: Normalize(42),
              color: '#000',
              fontFamily: 'Pacifico-Regular'
            }}>Swabi Surbey</Text>
            <TouchableOpacity onPress={this.SetAddNewScreen} style={{
              backgroundColor: '#cdcdcd',
              borderColor: '#000',
              borderRadius: GetWidth(5),
              borderWidth: 1,
              marginHorizontal: '10%',
              paddingHorizontal: '5%',
              paddingVertical: GetHeight(15),
              marginTop: GetHeight(50),
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center'
            }}>
              <Text style={{
                color: '#000',
                fontSize: Normalize(22),
                fontWeight: 'bold',
              }}>
                { strings.addNew }
              </Text>
              <Image source={PlusIcon} style={{ width: GetWidth(20), height: GetWidth(20) }} />
            </TouchableOpacity>
            <TouchableOpacity onPress={this.SetReportScreen} style={{
              backgroundColor: '#cdcdcd',
              borderColor: '#000',
              borderRadius: GetWidth(5),
              borderWidth: 1,
              marginHorizontal: '10%',
              paddingHorizontal: '5%',
              paddingVertical: GetHeight(15),
              marginTop: GetHeight(50),
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center'
            }}>
              <Text style={{
                color: '#000',
                fontSize: Normalize(22),
                fontWeight: 'bold',
              }}>
                { strings.reports }
              </Text>
              <Image source={ReportIcon} style={{ width: GetWidth(20), height: GetWidth(20) }} />
            </TouchableOpacity>
          </Paper>
        </WoodBackground>
      )
    }
    return (
      <PaperHolder
        actualStep={state.step}
        nextStep={nextStep}
        backStep={backStep}
        FinishAndCaptureData={FinishAndCaptureData}
        length={this.steps.length}
        scroll={canScroll}
        canContinue={state.canContinue}
      >
        {ActualStep}
      </PaperHolder>
    );
  }
}

export default SurveyScreen
