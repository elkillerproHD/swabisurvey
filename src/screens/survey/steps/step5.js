import React, { useState } from 'react';
import {Platform, Text, TextInput, View, StyleSheet} from "react-native";
import {GetHeight, GetWidth, Normalize, screenHeight} from "../../../utils/size";
import InputScrollView from 'react-native-input-scroll-view';


const strings = {
  question: 'Description of your technology needs',
  description: 'Type your description here'
};


function SurveyStep5 ( props ) {
  const [descrip, setDescrip] = useState('');
  function SetData (value){
    setDescrip(value);
    props.setData(value);
  }
  return(
      <View style={{ width: '100%', height: '100%', paddingHorizontal: '5%', alignItems: 'center'}}>
        <View style={{ width: '100%', height: screenHeight * 0.05}} />
        <Text style={{width: '100%', color: '#000', fontSize: Normalize(18), fontWeight: 'bold', textAlign: 'left'}}>
          {strings.question}
        </Text>
        <View style={{ width: '100%', height: screenHeight * 0.02}} />
        <TextInput
          placeholderColor={'#000'}
          placeholder={
            strings.description
          }
          style={styles.inputStyle}
          value={descrip}
          onChangeText={SetData}
          autoFocus={props.autoFocus}
          multiline
        />
      </View>

  )
}

const styles = StyleSheet.create({
  inputStyle: {
    paddingTop: GetHeight(10),
    marginTop: GetHeight(10),
    lineHeight: Normalize(16),
    fontSize: Normalize(14),
    borderColor: '#000',
    borderWidth: GetWidth(1),
    borderRadius: GetWidth(8),
    textAlign: 'left',
    textAlignVertical: 'top',
    zIndex: 1,
    color: '#000',
    width: '100%',
    padding: '3%',
    paddingVertical: screenHeight * 0.03
  },
});

export default SurveyStep5;
