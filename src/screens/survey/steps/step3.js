import React, { useState } from 'react';
import {Text, TextInput, View} from "react-native";
import {GetHeight, Normalize, screenHeight} from "../../../utils/size";
import ButtonSwitch from "../../../components/switch";
import {GoogleInput} from "../../../components/input/google";

const strings = {
  yes: 'Yes',
  no: 'No',
  name: 'Full Name',
  jobTitle: 'Job Title',
  company: 'Company',
  phone: 'Phone',
  email: 'Email',
  question: 'Complete the following form:'
};

function SurveyStep3 ( props ) {

  const [name, setName] = useState('');
  const [jobTitle, setJobTitle] = useState('');
  const [company, setCompany] = useState('');
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');

  function SetData (name, value, setFunction){
    setFunction(value);
    props.setData(name, value);
  }

  const inputs = [
    {
      value: name,
      placeholder: strings.name,
      type: 'default',
      setValue: (value) => {SetData('name', value, setName)}
    },
    {
      value: jobTitle,
      placeholder: strings.jobTitle,
      type: 'default',
      setValue: (value) => {SetData('jobTitle', value, setJobTitle)}
    },
    {
      value: company,
      placeholder: strings.company,
      type: 'default',
      setValue: (value) => {SetData('company', value, setCompany)}
    },
    {
      value: phone,
      placeholder: strings.phone,
      type: 'phone-pad',
      setValue: (value) => {SetData('phone', value, setPhone)}
    },
    {
      value: email,
      placeholder: strings.email,
      type: 'email-address',
      setValue: (value) => {SetData('email', value, setEmail)}
    }
  ];

  return(
    <View style={{ width: '100%', paddingHorizontal: '5%', alignItems: 'center'}}>
      <View style={{ width: '100%', height: screenHeight * 0.01}} />
      <Text style={{width: '100%', color: '#000', fontSize: Normalize(18), fontWeight: 'bold', textAlign: 'left'}}>
        {strings.question}
      </Text>
      {
        inputs.map((input) => (
          <GoogleInput
            key={`Input${input.placeholder}`}
            value={input.value}
            onChangeText={(value) => {input.setValue(value)}}
            placeholder={input.placeholder}
            padding={0}
            noClear
            keyboardType={input.type}
            containerStyle={{marginTop: GetHeight(10)}}
          />
        ))
      }

    </View>
  )
}

export default SurveyStep3;
