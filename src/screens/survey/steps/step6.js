import React, {useState} from 'react';
import {Platform, StyleSheet, Text, TextInput, View} from "react-native";
import {GetHeight, GetWidth, Normalize, screenHeight} from "../../../utils/size";
import ButtonSwitch from "../../../components/switch";
import {GoogleInput} from "../../../components/input/google";

const strings = {
  yes: 'Yes',
  no: 'No',
  question1: 'Who is your Chief Technology Officer?',
  name: 'Name'
};

const buttonsSwitch = [
  {
    name: strings.yes,
  },
  {
    name: strings.no,
  },
];

function SurveyStep6 ( props ) {
  const [ value, setValue ] = useState('');

  function SetData (value){
    setValue(value);
    props.setData(value);
  }

  return(
    <View style={{ width: '100%', paddingHorizontal: '5%', alignItems: 'center'}}>
      <View style={{ width: '100%', height: screenHeight * 0.05}} />
      <Text style={{width: '100%', color: '#000', fontSize: Normalize(18), fontWeight: 'bold', textAlign: 'left'}}>
        {strings.question1}
      </Text>
      <View style={{ width: '100%', height: screenHeight * 0.02}} />
      <GoogleInput
        value={value}
        onChangeText={SetData}
        placeholder={strings.name}
        padding={0}
        noClear
      />
    </View>
  )
}

export default SurveyStep6;
