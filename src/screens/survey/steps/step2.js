import React from 'react';
import {Text, View} from "react-native";
import {Normalize, screenHeight} from "../../../utils/size";
import ButtonSwitch from "../../../components/switch";

const strings = {
  yes: 'Yes',
  no: 'No',
  question1: 'Do you have a mobile app?'
};

const buttonsSwitch = [
  {
    name: strings.yes,
  },
  {
    name: strings.no,
  },
];

function SurveyStep2 ( props ) {
  return(
    <View style={{ width: '100%', paddingHorizontal: '5%', alignItems: 'center'}}>
      <View style={{ width: '100%', height: screenHeight * 0.05}} />
      <Text style={{width: '100%', color: '#000', fontSize: Normalize(18), fontWeight: 'bold', textAlign: 'left'}}>
        {strings.question1}
      </Text>
      <View style={{ width: '100%', height: screenHeight * 0.02}} />
      <ButtonSwitch
        onChange={(value, index) => { props.setData(index) }}
        selected={-1}
        buttons={buttonsSwitch}
      />
    </View>
  )
}

export default SurveyStep2;
