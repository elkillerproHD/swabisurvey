import React from 'react'
import {AsyncStorage, View, Text, TouchableOpacity, BackHandler} from 'react-native'
import {GetHeight, GetWidth, Normalize, screenHeight, screenWidth} from "../../utils/size";
import {constants as CONSTANTS} from "../constants";

class ReportScreen extends React.Component {

  toMap = [
    'name',
    'company',
    'jobTitle'
  ];

  titleColumns = [
    'Name',
    'Company',
    'Title'
  ];

  renderDetail = [
    {name: 'name', text: 'Name: '},
    {name: 'jobTitle', text: 'Job Title: '},
    {name: 'company', text: 'Company: '},
    {name: 'email', text: 'Email: '},
    {name: 'hasMobileApp', text: 'Has Mobile App? '},
    {name: 'hasWeb', text: 'Has Web? '},
    {name: 'phone', text: 'Phone: '},
    {name: 'wantToBeContacted', text: 'Want To Be Contacted? '},
    {name: 'descriptionTechNeeded', text: 'Description Tech Needed: '},
    {name: 'ceoName', text: 'Ceo Name: '},
  ];

  state = {
    data: false,
    detailScreen: false,
    detail: {},
    render: []
  };

  GetCookie = async cookieName => {
    let storageData = {};
    try {
      storageData = await AsyncStorage.getItem('report');
      storageData = JSON.parse(storageData);

      return Promise.resolve(storageData['report']);
    } catch (error) {
      console.log(`Hubo un error obteniendo la cookie: ${'report'}`);
      console.log(error);
      return Promise.reject(`Hubo un error obteniendo la cookie: ${'report'}`);
    }
  };

  componentDidMount() {
    this.backPress = BackHandler.addEventListener(
      'hardwareBackPress',
      this.BackPress
    );
    this.willBlurSubscription = this.props.navigation.addListener(
      'willBlur',
      () => {this.backPress.remove()}
    );
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.backPress = BackHandler.addEventListener('hardwareBackPress', this.BackPress)
        this.GetCookie('report').then((report) => {
          this.setState({
            data: report
          });
          this.RenderReport()
          console.log(report)
        });
      }
    );
    this.GetCookie('report').then((report) => {
      this.setState({
        data: report
      });
      this.RenderReport()
      console.log(report)
    })
  }

  BackPress = () => {
    const { state, props } = this;
    if(state.detailScreen){
      this.setState({
        detailScreen: false
      });
    } else {
      props.navigation.goBack();
    }
    return true;
  };

  SetDetailScreen = (detail) => {
    this.setState({
      detailScreen: true,
      detail
    })
  };

  RenderReport = () => {
    const {state, toMap} = this;

    const render = [];
    console.log("state date")
    state.data.map((data, index) => {
      let toContainer = [];
      console.log(data)
      toMap.map((mapp) => {
        console.log(data)
        console.log(mapp)
        console.log(data[mapp])
        toContainer.push(
            <View key={`ToContainer${mapp}`} style={{ width: '33%', justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: '#000', height: GetHeight(25), overflow: 'hidden' }}>
              <Text style={{ color: '#000', fontSize: Normalize(14), height: GetHeight(16) }} >
                { data[mapp] }
              </Text>
            </View>
        );
      });
      render.push(
        <TouchableOpacity key={`Container${index}`} onPress={() => {this.SetDetailScreen(data)}} style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between' }}>
          {[toContainer]}
        </TouchableOpacity>
      );
      toContainer = [];
    });
    console.log(render)
    this.setState({
      render
    })
  };

  render(){
    const {state, titleColumns, renderDetail} = this;
    console.log(state.detail)
    if(state.detailScreen){
      return(
        <View style={{ width: screenWidth, height: screenHeight, paddingHorizontal: '5%', paddingTop: '10%' }}>
          {
            renderDetail.map((data) => {
              if(data.name === "descriptionTechNeeded"){
                return(
                  <View style={{ width: '100%' }}>
                    <Text style={{ color: '#000', fontSize: Normalize(16), fontWeight: 'bold' }} >
                      { data.text }
                    </Text>
                    <Text style={{ color: '#000', fontSize: Normalize(16) }} >
                      { state.detail[data.name] }
                    </Text>
                  </View>
                )
              }
              return (
                <View style={{ width: '75%', flexDirection: 'row' }}>
                  <Text style={{ color: '#000', fontSize: Normalize(16), fontWeight: 'bold' }} >
                    { data.text }
                  </Text>
                  <Text style={{ color: '#000', fontSize: Normalize(16) }} >
                    { state.detail[data.name] }
                  </Text>
                </View>
              )
            })
          }
        </View>
      );
    }
    return(
      <View style={{ width: screenWidth, height: screenHeight, paddingHorizontal: '5%' }}>
        <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', paddingTop: '10%' }}>
          {
            titleColumns.map((data, index) => (
              <View key={`ColumnsTitle${index}`} style={{ width: '33%', justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: '#000', height: GetHeight(25) }}>
                <Text style={{ color: '#000', fontSize: Normalize(14), fontWeight: 'bold' }} >
                  { data }
                </Text>
              </View>
            ))
          }
        </View>
        {
          state.data !== false && (
            state.render
          )
        }
      </View>
    )
  }
}

export default ReportScreen
